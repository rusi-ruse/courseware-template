---
title: Home
nav_order: 1
description: "The main page of the Courseware as Code documentation."
permalink: /
---

# Courseware as Code

Welcome to Courseware as Code!